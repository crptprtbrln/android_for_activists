### Android für Aktivist*innen

#### Einführung
* Arten von Daten
* Wer überwacht uns?

#### Mobilfunk
* GSM - Wie funktionierts?
* Exkurs: Vorratsdatenspeicherung
* Überwachung
* Mobilfunküberwachung

#### Betriebssystem Android
* Basics
* Freies Android: LineageOS
* App Stores
* Sicherheit

#### Android für Aktivist*innen
* Messenger
* Exkurs: Staatstrojaner
* sonstige

