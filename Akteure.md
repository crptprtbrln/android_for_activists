### Wer uns überwacht (heute und morgen)

#### Konzerne
* Markt-/Trend-/Konsumanalysen
* Personalisierte Services
* Personalisierte Werbung
* Weiterverkauf (auch an )

#### Staat
* Strafverfolgung
* "Prävention"
* Struktur-/Netzwerkanalysen

#### Kooperationen
* Geschäftsmodell: Datensammlung und Verkauf an Behörden (z.B. Geofeedia)

???

64 Prozent der Google-Anfragen positiv

---

### Wer uns überwacht (heute und morgen)

#### Cracker/Crime
