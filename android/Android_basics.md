## Android

1. Was ist Android?

2. LineageOS

3. Woher bekomme ich meine Software?

4. Android Sicherheit

---
## Was sind eigentlich Smartphones?
In a 2003 interview with BusinessWeek, just two months before incorporating Android, [Andy] Rubin said there was tremendous potential in developing smarter mobile devices that are _more aware of its owner's location and preferences_. "If people are smart, that information starts getting aggregated into consumer products," said Rubin.

---
## Was ist Android?
* Betriebssystem [OS] für mobile Geräte (Mobiltelefone, Tablets, Netbooks, etc.)

* Mit über 80% Marktanteilen weltweit verbreitetstes OS für diese Geräte-Klassen _([Stand Oktober 2017](https://www.googlewatchblog.de/2017/10/kwp-android-deutschland-prozent/))_

---
#### Android als freie Software - in der Theorie...

* [Quelloffen](https://source.android.com/setup/downloading) unter der Führung der [Open Handset Allience](https://de.wikipedia.org/wiki/Open_Handset_Alliance), einem von Google initiierten Konsortium namhafter Tech-Konzerne entwickelt.

* Größten Anteil an der Entwicklung behält sich nach wie vor Google selbst vor

* Android ist im Kern freie Software, d.h. "mensch kann damit nach Belieben umgehen"
  * Kontrolle
  * Unbegrenzte Verwendbarkeit
  * Kopieren und weitergeben
  * Veränderbarkeit

* Versuche alternativer mobile-OS wie Firefox OS, Ubuntu, etc. basier[t]en alle auf Android

---
#### Android als freie Software - in der Praxis...
* Üblicherweise im Auslieferungszustand der meisten Geräte mehr oder weniger viele **nicht-quelloffene** Bestandteile
  - Gerätetreiber
  - Apps (zB GoogleApps, LifeTracker, Sprachsteuerung, etc.)

* Teilweise müssen erst **hohe Hersteller-Hürden** - eventuell sogar unter Verlust der Garantie (!) - überwunden werden um den vollen Umfang des OS nutzen zu können oder das Gerät von proprietärer Software zu beseitigen

---
#### Was ist das Problem mit nicht-quelloffener Software?
* Wir haben keine **Kontrolle** darüber, was die genutzte Software im Rahmen ihrer Privilegien alles tut.

* Auf folgende Fragen findet man - wenn überhaupt - meist nur mit hohem Aufwand eine Antwort

  - Sendet sie ohne unser Wissen **Metadaten** Server im Internet?
  - **Späht** sie ungeschützte Bereiche im System aus?
  - Tut die Software das was sie im Kern vorgibt zu tun auch wirklich (Verschlüsselung, etc)?
---
#### Was ist das Problem mit nicht-quelloffener Software?
* Die Installation nicht-quelloffener Software muss entsprechend als **riskant** bewertet werden

* Zentrale Fragen sind hierbei
  1. **Wer** verfasst die Software die wir nutzen?
  2. **Wer** bewertet ihre Funktionalität?
  3. **Woher** beziehe ich die Software, die ich nutzen möchte?

---
## LineageOS
* Durch breite Community [entwickeltes](https://github.com/LineageOS), freies und quelloffenes OS für mobile Geräte auf Basis von Android

* Nachfolger von bekannter Android-Modifizierung ("Custom ROM") [CyanogenMod](https://de.wikipedia.org/wiki/CyanogenMod)

* Vorteile
  - Im Gegensatz zu Hersteller-Auslieferungen von Android von Haus aus **keine** nicht-quelloffenen Bestandteile
  - Standardmäßig mehr security-features (zB Privacy Guard)
  - [Optionaler] Vollzugriff aufs System via Root-Rechte (festlegbar pro App)
  - Hersteller-unabhängiger support für neue Android-Versionen

* Nachteile
  - Nutzbarkeit abhängig vom Community-Einsatz eine Version für das betreffende Gerät zu produzieren
  - Unter Umständen Verzicht auf Funktionalität

---
## Woher bekomme ich meine Software? - **PlayStore**
* Meistgenutzter Android App-Store
* Enthält OpenSource und closed-Source-Software

**Nachteile**
* Erfordert die Installation zusätzlicher Software von Google (-> "Google Libs")
* Viele Apps mit Schadsoftware, Datenschleudern, etc.

---
.center[![SonicSpy found in PlayStore](img/android/sonicspy.png)]

---
#### How Google fought bad apps and malicious developers in 2017

* Allein 2017 hat Google [über 700.000 Apps aus dem Playstore](https://android-developers.googleblog.com/2018/01/how-we-fought-bad-apps-and-malicious.html) entfernen müssen, bei denen Schadcode festgestellt wurde (Anstieg von 70%)

* Google behauptet, dass 99% der Apps entfernt wurde bevor überhaupt eine Person diese installieren konnte

* Dennoch bleiben über 7.000 schadhafte Apps mit wenigstens einer Installation


---
### Woher bekomme ich meine Software? - **FDroid**
* App-Store-System zur Verbreitung von Android-Software-Paketen
* Offizielle Paketquellen des FDroid-Projekts enthalten ausschließlich i[]Free Open Source Software (FOSS)](https://de.wikipedia.org/wiki/Foss)
* Keine zusätzliche Software außer die App-Store Software selbst erforderlich

**Nachteile**
* Nicht-kommerzielles Projekt: Teilweise alte / veraltete Versionen
* Community-basiert betriebenes
* Alle Apps werden mit ein und demselben Schlüssel (vom FDroid-Projekt) unterschrieben (Sicherheit / Vertrauen!)

---
### FDroid vs. Playstore
* Auf einem Google-befreiten Gerät ist die direkte Nutzung vom Google PlayStore **keine** Option
* Freiheitsliebende Menschen suchen am Ehesten nach Software via FDroid
* ABER:
  * Nicht alle Apps befinden sich im FDroid-Store (teilweise closed-Source, etc.)
  * Teilweise sind die Versionen älter als im PlayStore

---
### Kompromiss: FDroid + Yalp Store
* Kompromiss: FDroid verwenden, alles was nicht über FDroid bezogen werden kann über Yalp Store beziehen
* Mittels [Yalp Store](https://f-droid.org/en/packages/com.github.yeriomin.yalpstore/) kann man Pakete direkt aus dem Google PlayStore runterladen

**Vorteile**
* Anonymes runterladen von Paketen möglich
* Möglichkeiten des automatischen Updatens
* Nutzen des PlayStores auch ohne die Verwendung systemweiter Bibliotheken von Google

**Nachteile**
* Evtl. Verwirrung: Für welche Pakete ist FDroid, für welche Yalp zuständig?
