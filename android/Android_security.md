### Sicherheit und konkrete Angriffe auf Android-Devices

* Welche Android-Schadsoftware gibt es und wird staatlicherseits genutzt?
* Bildschirmsperre - Was bringt sie?
* Device-Verschlüsselung - Wie gut ist sie?

---
## Android Sicherheit
### Vollverschlüsselung
https://www.heise.de/security/meldung/Heftiger-Schlag-fuer-Android-Verschluesselung-3254136.html

* Frage ist: Wo liegen die Schlüssel?
---

### Displaysperre
* Auf keinen Fall Fingerabdruck, potentiell darf Polizei ED-behandeln und hat damit Fingerabdrücke, biometrische Scanner schwach und überwindbar
* Auf keinen Fall pattern lock
---

## Geräteverschlüsselung
---

### Geräteverschlüsselung
* Euer Gerät wird bei einer Hausdurchsuchung, Aktion, etc. durch Beamte konfisziert

* Im **unverschlüsselten Zustand** hat die Forensik keinerlei Arbeit

* Das Leben erheblich schwerer macht die **Verschlüsselung** eures Devices

* **Seit Android 5**: [Full disk encryption]() (FDE)

* **Seit Android 7**: FDE und - geräteabhängig - [File based encryption]() (FBE)

???

* Viele ältere Devices unterstützen FBE NICHT

---
### Geräteverschlüsselung (sehr grob!)
_Behandelt wird hier ausschließlich das "ältere" FDE Verfahren_

* Der Inhalt eures Geräts wird mithilfe einer Passphrase (PIN, Pattern, Passwort, Fingerprint) unter Zuhilfenahme des Prozessors verschlüsselt

* Die Kombination generiert einen Schlüssel, der von Android **zur Laufzeit im RAM** eures Geräts gespeichert wird

* Zur Laufzeit sind die Daten entschlüsselt, der Zugriff wird durch die Displaysperre durch die Passphrase verhindert

* Pro Start des Systems werdet ihr um die Eingabe dieser Passphrase gebeten, eine erfolgreiche Eingabe entschlüsselt eure Daten für die gesamte Laufzeit

???

* [Hier Details des Encryption / Decryption Verfahrens]

---
## Angriffe auf die Geräteverschlüsselung
_Die folgenden Szenarien sind lediglich theoretischer Natur und repräsentieren nicht die gängigen Praxen deutscher Behörden_

---
### Angriffe - "Online-Brute-Forcing"
* Gerät befindet sich zum Zeitpunkt des Angriffs im **angeschalteten** Zustand

* Automatisiertes "Durchtesten" der Passphrase bis das Gerät entschlüsselt ist

* **Android 6**: Androids **Rate-limiting** (""Versuche bis zur Eingabe-Sperre"") ist schwach, 4-stellige PIN in [19-22h erratbar](http://www.delaat.net/rp/2016-2017/p45/report.pdf)

* **Android 7**: Machts etwas besser, 4-stellige PIN ~ 27 Jahre

_Die Wahrscheinlichkeit, dass ein Gerät zum Zeitpunkt einer vermeintlichen Konfiszierung (Hausdurchsuchung, Aktion, etc.) angeschaltet ist ist relativ hoch!_

???

* Im engeren Sinne kein direkter Angriff auf sondern eher "Umgehung" der Verschlüsselung (sog. "side-chaining")

* **Technische Verfahrensweise**: Im angeschalteten Zustand via Brute-Forcing zB via ADB auf externem Host oder spezieller plugged-in Hardware am Gerät selbst (USB OTG ist im gelockten Zustand verfügbar!)
* Nachfrage: Erfahrungen mit Hausdurchsuchungen und angeschalteten Geräten (insb. Laptops und Tablets)

---
### Angriffe - "Cold-Boot Angriff"
* Gerät befindet sich zum Zeitpunkt des Angriffs im **angeschalteten** Zustand

* Encryption keys für [Full disk encryption](https://source.android.com/security/encryption/) (ab Android 5.0) sind im RAM aufgehoben

* RAM besitzt die Eigenschaft, dass sein Inhalt nach Unterbrechen der Stromversorgung (zB durch Ausschalten oder Reboot) **nicht sofort geleert wird** sondern langsam mit der Zeit verschwindet.

* Je **kälter** das RAM-Modul ist desto langsamer entleeren sich die Chips

* Konkrete Angriffe sind für alle vollverschlüsselten Geräte möglich, die nicht die neuere [File based encryption](https://source.android.com/security/encryption/file-based) (FBE) nach der Spezifizierung von Google anbieten

???

* Ein mögliches Verfahren wird [hier](https://www1.informatik.uni-erlangen.de/frost) skizziert

* **Technische Verfahrensweise** 

  * **Vorraussetzung**: Entsperrter bootloader!

  * Das Gerät wird auf 5-10° heruntergekühlt, Stromzufuhr für möglichst kurzen Moment unterbrechen (kurz Batterie rausnehmen)

  * Anschließend wird das FROST recovery image aufgespielt und der RAM ausgelesen

  * Ist der **Bootloader gesperrt** muss er zunächst entsperrt werden, was zum **Verlust der Datenpartition** führt!

---
### Angriffe - "Hausangestellen-Angriff"
* Gerät befindet sich zum Zeitpunkt des Angriffs im **angeschalteten** Zustand

* Typisches Angriffsszenario ist das Verlassen des Raums **ohne das unbeaufsichtigte Gerät**

* Ein Angreifer spielt [Software](http://docs.huihoo.com/blackhat/europe-2014/eu-14-Artenstein-Man-In-The-Binder-He-Who-Controls-IPC-Controls-The-Droid-wp.pdf) auf das Gerät, welches die Displaysperre derart modifiziert, dass diese die Eingabe mitprotokolliert und nach Vorhandensein einer Internet-Verbindungen verschickt

???

* **Technische Verfahrensweise** 

  * Diese Attacke ist möglich sofern die FDE lediglich die Daten-Partition verschlüsselt und der **Bootloader entsperrt** ist

  * Ist der **Bootloader gesperrt** bleibt die Möglichkeit, das das Gerät gegen ein identisches mit entsperrtem Bootloader ausgetauscht wird

---
### Angriffe - "Fingerabdruck"
* Gerät befindet sich zum Zeitpunkt des Angriffs im **angeschalteten** oder **angeschaltetem** Zustand

* Fingerabdrücke können euch unter Zwang einfacher abverlangt werden als Passphrasen (physischer Zugriff auf euren Körper!)

* Eure Fingerabdrücke sind ggf. bereits in einer Datenbank gespeichert und Biometrie im Allgemeinen [überlistbar](https://www.ccc.de/de/campaigns/aktivitaeten_biometrie/fingerabdruck_kopieren)

* Gerootete Telefone sind angreifbar gegenüber [anderen Formen von Attacken](https://homepages.staff.os3.nl/~delaat/rp/2015-2016/p30/report.pdf)

---

### Angriffe - "Offline-Angriff"
* Gerät befindet sich bereits eine Weile im **ausgeschalteten** Zustand

* Ausnutzen einer Sicherheitslücke innerhalb von Android und der Architektur von Qualcomm-Prozessoren ermöglicht es den Encryption Key der FDE auszulesen

* Anschließend muss lediglich die Passphrase geraten werden

* Hier gilt: Längere Passphrasen schützen besser gegen eine anschließende Brute-Force-Attacke

* Problem: Passphrase für Displaysperre und Verschlüsselung sind gleich, viele User wählen daher **tendenziell zu kurze Passphrasen**

???

* Angreifende Person **weiß wahrscheinlich zunächst nichts** über die Gestalt der Passphrase (PIN, Password, Pattern?)

* **Pattern Lock** ist generell eine Möglichkeit den Zeichenraum zu vergrößeren und eine Brute-Forcing Attacke zu verunmöglichen

* **Achtung**: Forscher*innen haben eine effektive Möglichkeit zur [Detektion des Pattern-Lock aus Videomaterial](http://www.lancaster.ac.uk/staff/wangz3/publications/ndss_17.pdf) entwickelt

* **Aber**: Diese Möglichkeiten gibt es allerdings genauso für PIN und Passwort

---

### Fazit / Empfehlungen
* Keine strafrechtlich relevanten Inhalte, auch nicht auf verschlüsselten Geräten!

* **Sicherheitspatches installieren**

* Verwendet **keine Fingerabdrücke** zum Ver- und Entsperren eures Geräts

* Wurde euer Gerät konfisziert solltet ihr erwägen es danach mindestens **neu aufzusetzen**!

* Wenn ihr den Einbau unerwünschter Hardware ausschließen wollt solltet ihr das Device bei fehlender technischer Expertise fachgerecht planebnen

* Ein **entsperrter Bootloader** sollte nach dem Aufspielen von Custom ROMs wieder gesperrt werden

* **Root-Zugriff** ist ein Sicherheitsrisiko und sollte als Teil einer Abwägung betrachtet werden
