## Android-Software für Aktivist\*innen: **Messenger**

### Standardvariante: Zentrale Infrastruktur
Zentraler Server, der Nachrichten deligiert

**Nachteile:**
* Zentrale Metadaten
* Server/Provider muss vetraut werden
* Ausfallgefahr
* Definierter Angriffspunkt für Hacker

### Worauf kommt es im aktivistischen Kontext an?
* Ende-zu-Ende-Verschlüsselung (+ "perfect forward secrecy")
* Datensparsamkeit
* Software muss vertraut werden können: Offener Quellcode!

---

### Messenger - **Whatsapp / Telegram**

**Beide Messenger sind nicht zu empfehlen!**

* Telegram verschlüsselt standardmäßig **nicht** - Verschlüsselung für Gruppenchats garnicht möglich

* Telegram benutzt eigenes, nicht quelloffenes Verschlüsselungsprotokoll

* Whatsapp verschlüsselt, zumindest Metadaten werden aber sicherlich verwertet (Whatsapp gehört zu Facebook!)

* Adressbuch wird an amerikanischen Server gesendet

???

"Denn wer Whatsapp nutze, erlaube dem Dienst, alle Kontaktdaten seines Smartphones auszulesen. Diese Zustimmung dürfe jemand Whatsapp aber nur geben, wenn alle Menschen, die im Adressbuch des Nutzers stehen, dem zugestimmt hätten.
Da es solche vollständigen Einwilligungen aber in der Realität praktisch nie gebe, sei die Nutzung des Dienstes in der Regel rechtswidrig, sagte Hasse. Diese Sicht von Datenschützern habe 2017 auch das Amtsgericht Bad Hersfeld in einem Urteil bestätigt. "Diese Entscheidung betrifft alleine Privatleute", sagte Hasse."


It is proven that Skype even parses user data!!

---

### Messenger - **Signal**

#### Security features
* Alles ist standardmäßig Ende-zu-Ende-verschlüssselt
* Verschlüsselung nach "Trust-on-first-use"-Prinzip
* Schlüsselabgleich erfolgt optional (manuell oder QR-Code)
* Auch Verschlüsselung von Bildern, Sprachnachrichten, Telefonaten etc.
* Optionale Verschlüsselung des lokalen Speichers über Passwort
* Open Source, of course

#### Die Macher*innen von Signal - Wer ist Open Whisper Systems?
* Whisper Systems - gegründet 2010 von Sicherheitsforscher Moxie Marlinspike und Robotiker Stuart Andersen
* finanziert durch private Spenden und verschiedenen Funds/Programmen
* Februar 2018: WhatsApp-Mitbegründer gründet die Signal Foundation und investiert 50.000.000$ -> erste bedeutende Investion in Signal!

---

### Messenger - **Signal**

#### Anfallende Metadaten
* OWS speichert nach eigener Aussage keine Logs von Verkehrsdaten (wer kontaktiert wen und wann)
* Es wird lediglich gespeichert, wer an welchem Tag (kleinste zeitliche Auflösung) sich mit dem Signal-Server verbunden hat
* Fraglich ist, welche Daten durch die Nutzung von [Google Push Notifications](https://developers.google.com/web/ilt/pwa/introduction-to-push-notifications) anfallen

#### Sonstige Nachteile
* Contact discovery via Telefonbuch (seit einiger Zeit aber über ein sichereres System - SGX)
* Zentralisierte Infrastruktur - Vertrauen, dass OWS auch den Server-Code hosted den sie [OpenSource veröffentlichen](https://github.com/signalapp/Signal-Server)


???

* Wikipedia zu Metadaten: "Die signierte App verwendet zur Kommunikation den Dienst Google Cloud Messaging (GCM). Durch GCM wird dabei nur das Vorhandensein einer neuen Nachricht signalisiert, jedoch nicht deren Inhalt oder Absender. Diese Information kann von Google gespeichert werden, ist jedoch für sich genommen relativ wertlos. Die eigentlichen Nachrichten werden über die Server von Open Whisper Systems übermittelt und sind Ende-zu-Ende-verschlüsselt."

* we’ve published a new private contact discovery service.
Using this service, Signal clients will be able to efficiently and scalably determine whether the contacts in their address book are Signal users without revealing the contacts in their address book to the Signal service.

---

### Messenger - **Signal**

#### Woher bekomme ich Signal?

* FDroid - können wir FDroid vertrauen?

* Direct Download - Wollen wir manuell updaten?

* Google PlayStore - Wollen wir Google benutzen?

* Yalp Store (PlayStore Proxy)

???

## Woher bekomme ich Signal
### FDroid vs. OWS
Packages in FDroid werden pro Repository mit einem zentralen Key gesigned. Moxie [kritisierte bereits 2013](https://github.com/signalapp/Signal-Android/issues/127#issuecomment-13447074) dieses Vorgehen und ist scheinbar auch nach wie vor nicht bereit, Signal via FDroid auszuliefern

>  I totally agree that 3rd party app store alternatives seem cool, but until they're done securely and correctly, I'd prefer not to participate.

Der Punkt ist, dass man FDroid **vertrauen muss**, dass
1. deren Infrastruktur nicht kompromitiert ist und/oder
2. FDroid auch das delivered, was die Entwickler*innen produziert haben (und keinen eigenen Schadcode hinzufügt).

Im [weiteren Verlauf der Debatte](https://github.com/signalapp/Signal-Android/issues/127#issuecomment-21763521) grenzt er jedoch die Bedinungen, unter denen er Signal unter FDroid verbreiten würden auf eher Entwicklungsprozess-orientierte Aspekte ein:

>  This is the current list of things that we need before we can distribute an APK outside of the Play Store. Any help completing these missing pieces would certainly be appreciated:

    A built in crash reporting solution with a web interface that allows us to visualize crashes and sort by app version, device type, etc. This is essential for producing stable software.
    A built in statistics gathering solution with a web interface that allows us to visualize aggregate numbers on device type, android version, and carriers for our users. This has been crucial in shaping support and development direction.
    A built in auto-update solution. Fully automatic upgrades won't be possible outside of Play Store, but we at least need something that will annoy the hell out of users until they upgrade. This is necessary for ensuring that new security features and bug fixes can be propagated quickly.
    A build system that allows us to easily turn these features on and off for Play and non-Play builds. Gradle should make this easier.

Des weiteren wurden [drei unterschiedliche Möglichkeiten](https://github.com/signalapp/Signal-Android/issues/127#issuecomment-26718635) der Signal-Distribution via FDroid aufgezeigt:

> Regarding moxie's need of security: He could provide his users with non-Gplay downloads in different ways:

    Setting up a binary f-droid repo (you build, you sign, f-droid only updates the index)

    Setting up a binary f-droid repo with automated signatures (you build, f-droid signs and updates the index)

    Setting up a source f-droid repo (f-droid builds, signs and updates the index)

### Signal über Google beziehen
* Die Verwendung von Google Playstore ist auf einem Google-befreiten Handy nicht [direkt] möglich

### Signal über "Proxy-Stores" beziehen
**Vorteile**
* Kontrolle über Build- und Auslieferungsprozess - kein Vertrauen gegenüber Drittanbieter (zB AppStore) notwendig
* Keine Installation von Google Playstore notwendig


### Signal aus dem Source-Code bauen
https://github.com/signalapp/Signal-Android/wiki/How-to-build-Signal-from-the-sources

**Vorteile**
* Kontrolle über Build- und Auslieferungsprozess - kein Vertrauen gegenüber Drittanbieter (zB AppStore) notwendig
* Keine Installation von Google Playstore notwendig

**Nachteile**
* Erfordert Expertise: Keine Form der massentauglichen Paketverbreitung


### Direktes Runterladen der APK von OWS
https://signal.org/android/apk/

**Vorteile**
* Kontrolle über Build- und Auslieferungsprozess - kein Vertrauen gegenüber Drittanbieter (zB AppStore) notwendig
* Alternative zur massentauglichen Verbreitung von Signal abseits von App-Stores

**Nachteile**
* Kein Auto-Update-Mechanismus - Unter Umständen wichtige Security-Features gelangen nicht [zeitnah] aufs Gerät~
  * Dazu [Kuketz Blog](https://www.kuketz-blog.de/empfehlungsecke/#messenger): _Für Fortgeschrittene: Ihr könnt die Signal-App direkt als APK herunterladen und den Google Play Store damit vermeiden. Signal prüft automatisch, ob Updates bereitstehen._
* Vertrauen gegenüber OWS, dass deren Webseite nicht kompromittiert wurde

---

### Es geht sicherer - Föderierte Protokolle
Standardisierte Protokolle, die es erlauben, über unterschiedliche Provider miteinander zu kommunizieren (Email ist eines davon)

* weniger Metadaten (zumindest weniger zentral)

* weniger Ausfallrisiko

* schwieriger zu attackieren

* "Selbstbestimmung"

## --> XMPP (a.k.a. Jabber)

* **Kontalk** -> keine Gruppenchats :(

* **Conversations**

* ...

Komfortnachteil: Eigener Account muss erstellt werden

???

There is also the more general problem that a privately run centralized service can decide which features to add independently of whether its users actually consider them features or maybe “anti-features”, e.g. telling other users whether you are “online” or not.

 I do concur that it makes it more difficult for good features to quickly be available for most people, but as mentioned previously, I think that from a privacy and security point of view it is clearly a feature, because it involves more people and weakens the possibility of the provider pushing unwanted features on the users

---

### XMPP-Messenger - Conversations

#### Security Features
* Unterschiedliche Möglichkeiten der Verschlüsselung insbesondere [OMEMO](https://conversations.im/omemo/)
  * Basis ist das [Signal-Verschlüsselungs-Protokoll](https://signal.org/blog/advanced-ratcheting/)
  * Asynchrone Kommunikation via XMPP wird damit möglich
  * Perfect forward secrecy
  * Future Secrecy

* "Trust-on-first-use" ist möglich, Schlüsselabgleich erfolgt dann optional (manuell oder QR-Code)

* Tor: Nutzung von Conversations als vollständig anonymisierte Messenger-Lösung möglich

---


### XMPP-Messenger - Conversations

#### Nachteile
* In den meisten Fällen < 20 MB/Monat upload-Volumen

* SicherheitsVerschlüsselung muss pro Chat pro Teilnehmer*in aktiviert werden

* Vertrauen in die Integrität und Kompetenz gegenüber unbekannten XMPP-Service-Betreiber*innen muss entgegengebracht werden

#### Anfallende Metadaten
* Abhängig von der Konfiguration des Service

#### Woher bekomme ich Conversations?
* [Fdroid](https://f-droid.org/en/packages/eu.siacs.conversations/)

* Playstore (hier kostenpflichtig!)

---

## XMPP Server
* Gute Übersicht [hier](https://datenschutzhelden.org/serverliste/)

* Für Aktivist*innen empfehlenswert: jabber.systemli.org (Registrierung [hier](https://jabber.systemli.org/register_web))

  * Logs werden vor dem Speichern von IP-Addressen bereinigt

  * Tor hidden-service

  * Logs sind "Warning / Error only" und werden aktuell (26.02.2018) 1 Woche vorbehalten

---

## Der Staatstrojaner
### Was ist das?
* auch Quellen-Telekommunikationsüberwachung (Quellen-TKÜ)

* Daten werden direkt von der Quelle geholt, da "Abfangen" von Nachrichten aufgrund Ende-zu-Ende-Verschlüsselung keine Inhalte verrät

* Dafür wird eine Schadsoftware auf dem Endgerät (PC, Smartphone...) installiert

* Polizei setzt große Hoffnungen in diesen Trojaner. Sie sagt, dass sie ohne dieses Werkzeug aufgrund Verschlüsselung zunehmend im Dunkeln tappt

???

"Noch nie gab es in der Geschichte der Bundesrepublik einen größeren, umfassenderen, weitreichenderen, heimlicheren und gefährlicheren Grundrechtseingriff: Das Bundeskriminalamt hat damit begonnen, sogenannte Staatstrojaner auf privaten Computern, Laptops und Handys zu installieren. Damit können sämtliche Daten ausgeleitet, damit kann das gesamte Computer-Nutzungsverhalten eines Menschen in Gegenwart und Vergangenheit überwacht werden."
[Heribert Prantl, SZ](http://www.sueddeutsche.de/digital/ueberwachung-der-staatstrojaner-frisst-die-grundrechte-auf-1.3842098)

---

## Der Staatstrojaner
### Technik
* nicht so einfach möglich, Software auf Geräten Dritter zu installieren

* Szenarien: Email mit Trojaner, Sicherheitscheck Flughafen, Hausdurchsuchung, Festnahme, Webseiten (siehe unten)...

### aktuelles
* wird seit Kurzem offiziell [eingesetzt](http://www.sueddeutsche.de/digital/ueberwachung-polizei-spioniert-handynutzer-mit-trojaner-aus-1.3842439)

* Verfassungsbeschwerde ist wohl auf dem Weg - viele Quellen sprechen von klarer Verfassungswidrigkeit

---

## Aktueller Trojaner für Android: **"Skygofree"**
> "Verbreitet wird die Software laut Kaspersky Lab über infizierte Websites, die aussehen wie die von Mobilfunkanbietern. Wer die Seiten aufruft, wird aufgefordert, sein Gerät zu aktualisieren oder neu zu konfigurieren - das sorgt dann für die Installation der Malware. Da dem Zugriff auf die Schnittstelle explizit zugestimmt werden muss, blendet die Malware eine harmlos klingende Aufforderung ein."

* So gut wie alles kann gesteuert, gelesen und gesendet werden

* [Kapersky mit Screenshot](https://www.kaspersky.de/blog/skygofree-hochentwickelte-spyware-seit-2014-aktiv/15645/)

---

<center><img src="img/mobilfunk/skygofree.png" width="500"></center>


---

### Und sonst so?
* Tor / VPN

* Mail

* Firewalls

* Kamera-Apps

* ...
