## Grundlagen Mobilfunk
---
### GSM
* Fast überall auf der Welt funktioniert Mobilfunk über GSM (2G-Netz)

* Vorraussetzung dafür ist die Benutzung eines GSM-kompatibles Telefons und einer **SIM-Karte**. Diese enthält unter Anderem:

  a) **IMSI:** Eine weltweit einzigartige Kennung

  b) **KC:** private Schlüssel zur Transportverschlüsselung der Verbindungen

* Seit Anfang Juli 2017 ist in Folge der [Auswertung der sog. Anti-Terror-Gesetzgebung](https://netzpolitik.org/tag/anti-terror-paket/) die **Freischaltung einer SIM-Karte** in Deutschland auf offiziellem Wege nur unter [Angabe der Personalien](https://netzpolitik.org/2017/interaktive-karte-registrierungspflicht-fuer-prepaid-sim-karten-in-europa-weit-verbreitet/) möglich

* Es gibt noch Restbestände bereits aktivierter Karten, Bestellung in [anderen europäischen Ländern](https://umap.openstreetmap.fr/en/map/prepaid-sim-card-registration_155856#6/51.000/2.000) könnte von Fall zu Fall Abhilfe schaffen

### GSM - SIM-Karten
* SIM-Karten [sind Computer](https://media.defcon.org/DEF%20CON%2021/DEF%20CON%2021%20video%20and%20slides/DEF%20CON%2021%20Hacking%20Conference%20Presentation%20By%20Karl%20Koscher%20and%20Eric%20Butler%20-%20The%20Secret%20Life%20of%20SIM%20Cards%20-%20Video%20and%20Slides.m4v)!

* Haben ein bisschen Speicher und einen Prozessor, der spezfische Rechenaufgaben übernehmen kann (zB Ver- und Entschlüsselung von Verkehrsdaten)

* Man kann mit ausreichend technischem Sachverstand eigene Anwendungen für SIM-Karten schreiben ("Applets"), die völlig unabhängig vom Betriebssystem operieren

* Provider haben technisch die Möglichkeit per Fernzugriff Anwendungen zu installieren

* Die SIM-Karte ist direkt mit dem sog. Baseband verbunden

---
### GSM-Verbindung - Grob vereinfacht
.center[![GSM simpel](img/mobilfunk/GSM_simple.png)]

???

### GSM-Verbindung - Verbindungsaufbau und Metadaten
* Um die Mobile Station beim Netz zu identifizieren wird die IMSI (International Mobile Subscriber Identity) benötigt

* Anfrage wird über Infrastruktur des Mobilfunkbetreibers weitergeleitet, bei dem entsprechende Verkehrsdaten anfallen
  * Wer mit wem
  * Über welche Funkzellen
  * Wie lange
  * ...

---
### GSM-Verbindung - Zustände
* In GSM kann ein Gerät folgende Zustände haben

1. **Aktiv:** Aktive Datenübertragung (SMS oder Gespräch), mittels untersch. Verfahren Ortsbestimmung möglich ([5m - mehrere km](https://de.wikipedia.org/wiki/GSM-Ortung))

2. **Offline:** ...

3. **Inaktiv:** Keine aktive Verbindung, keine genauere Ortsbestimmung als die sog. [Location Area](https://de.wikipedia.org/wiki/Location_Area) möglich. Erst wenn man ins Gebiet einer anderen LA kommt erfolgt ein Update über eine aktive Verbindung und es fallen neuerliche Informationen über den Aufenthalt des Endgeräts an

---
### Angriffe auf GSM - Ortung und Bewegungsprofile
* [Unterschiedliche Verfahren](https://de.wikipedia.org/wiki/GSM-Ortung) bieten hierbei **abhängig von der Dichte der Mobilfunknetz-Infrastruktur** unterschiedliche Genauigkeiten von mehreren Metern bis zu einigen Kilometern

* Voraussetzung dafür ist eine **aktive Verbindung** zu einer Funkzelle ist eine Ortung im Umkreis der Funkzelle möglich

* Für Behörden ergibt sich entsprechend im Zeitraum des **inaktiven** Zustands eines beobachteten Geräts eine zeitliche Lücke im Bewegungsprofil

* Von Behörden zum Tracking unterstützend eingesetztes Verfahren ist die sog. **[Stille SMS](https://de.wikipedia.org/wiki/Stille_SMS)**


---
### Angriffe auf GSM - Stille SMS
* Unbemerkt zugestellte SMS ("erscheint nicht auf dem Display")

* Beim Mobilfunkbetreiber fallen beim Empfang einer stillen SMS durch das überwachte Gerät Verkehrsdaten an und bilden so ein - je nach Beprobung und Funkzellendichte - umfangreiches Bewegungsprofil ab

* Rechtlicher Rahmen des Einsatzes ist juristisch umstritten, Grundlage ist uA §100 Strafprozessordnung

* **Verteidigung**
  * Gerät ausschalten ("nicht im Netz eingewählt sein")
  * Detektion evtl. via [SnoopSnitch](https://f-droid.org/packages/de.srlabs.snoopsnitch/)

* **Anwendung**
  * Sehr rege Praxis durch Zollfahndung, LfVs, BKA, LP, ...

???

### Angriffe auf GSM - Einsatz von stillen SMS 2010 - 2017
* SnoopSnitch setzt ein [kompatibles Smartphone](https://opensource.srlabs.de/projects/snoopsnitch/wiki/DeviceList) vorraus

---
### GSM - Verschlüsselung
* Grundlegend ist der Inhalt von SMS und Telefongesprächen [schwach] transportverschlüsselt

* Die Informationen liegen im **Klartext** beim Provider und können dort im Rahmen der **Vorratsdatenspeicherung** für eine gewisse Zeit aufbewahrt werden

* Es ist jedoch auch für zivile Personen mit dem nötigen Sachverstand möglich die GSM-Verschlüsselung zu brechen!

---
### Exkurs - Vorratsdatenspeicherung
_Stand: 1. März 2018_

#### Gegenwärtige Situation

* **Gesetzesbeschluss zum 1. Juli 2017**: Provider sollen **Standortdaten** aller Bürger für vier und deren **Kommunikations- und andere Verbindungsdaten** für bis zu zehn Wochen speichern

* Das [aktuell beschlossene Gesetz zur Vorratsdatenspeicherung](https://www.datenschutz.org/vorratsdatenspeicherung/) (VDS) wird in der Umsetzung von den Providern **nicht** mitgetragen

* **Kosten:** Sollen auf Provider ausgelagert werden, Umbau der Infrastruktur zur Umsetzung der VDS ist teuer

* **Klageverfahren laufen noch**: Unklar ob das Gesetz verfassungswidrig ist und deswegen früher oder später wieder abgeschafft werden muss

???

#### Das Wichtigste zur Vorratsdatenspeicherung in Kürze
von: https://www.datenschutz.org/vorratsdatenspeicherung/

* Ab dem **01. Juli 2017** sollte die Vorratsdatenspeicherung einsetzen. Alle Provider von Telekommunikationsdiensten waren eigentlich ab diesem Tag dazu angehalten, Verbindungs- und Standortdaten über einen längeren Zeitraum zu speichern. Am **28. Juni 2017** jedoch erklärte die Bundesnetzagentur die Aussetzung der Vorratsdatenspeicherung bis zum ordentlichen Abschluss eines Hauptsacheverfahrens.

* Die Standortdaten aller Bürger sollen für vier Wochen, deren Kommunikations- und andere Verbindungsdaten bis zu zehn Wochen vorsorglich gespeichert werden.

* Noch immer sind unzählige Klagen vor dem Bundesverfassungsgericht anhängig.

---
### Exkurs - Vorratsdatenspeicherung FAQ
_Stand: 1. März 2018_

**Gibt es momentan eine providerseitig verpflichtende Vorratsdatenspeicherung oder ist die Speicherung optional?**

_Die Pflicht zur Speicherung - im neuerlich beschlossenen Umfang - ist derzeit ausgesetzt_

**Wie lang müssen Provider die Daten vorrätig halten / wie lange tun sie es?**

* _Wer für seine Leistungen Geld verlangt, muss Daten erfassen, damit es eine Grundlage und Nachweisführung zur Rechnungslegung gibt_

* _Das heisst, die meisten speichern mindestens vier Wochen, einige aber auch länger_

---
### Exkurs - Vorratsdatenspeicherung FAQ
_Stand: 1. März 2018_

**Auf welcher rechtlichen Grundlage geschieht der Zugriff auf die vorratsgespeicherte Daten durch die Behörden?**

_Je nach Behörde existieren unterschiedliche Grundlagen._

* _Geheimdienste haben weitgehende Befugnisse ohne weitere Gründe._

* _In vielen aktuellen Polizeigesetzen der Länder (bspw Bayern) wird auch für Polizeien die Befugnis erweitert_

---
### Exkurs - Vorratsdatenspeicherung FAQ
_Stand: 1. März 2018_

**Inwiefern ist die Zusammenarbeit zwischen Providern und Behörden hierzulande gängig?**

* _Man kann davon ausgehen, dass die Zusammenarbeit inzwischen gängig ist_

* _Abfragen wie im Beispiel **Dresden Nazifrei** - wo Massenabfragen erfolgten - sind heute vermutlich keine Ausnahme mehr_

* _**Aber**: Auch hier ist die Datenlage gering_

???

_In Berlin gibt es dazu regelmäßig Anfragen, da ist ein massiver Anstieg bewiesen._

_In anderen Ländern weiß man es einfach nicht._

---
### Angriffe auf GSM - IMSI Catcher
* Telefon muss sich mithilfe der Informationen auf der SIM-Karte gegenüber Netz authentifizieren, das **Netz allerdings nicht gegenüber dem Gerät**

* Entsprechend möglich ist der Einsatz von sog. [IMSI-Catchern](https://de.wikipedia.org/wiki/IMSI-Catcher) zum Abfangen (und anschließendem Weiterreichen) von Verbindungen

* **Simulation einer Funkzelle**, in die sich ein Endgerät einwählt
* Aufzeichnen der relevanten Verkehrsdaten und Weiterreichen der Verbindung an eine reguläre Basisstation weiter

* Abhören von Telefongesprächen [ist möglich] und [wird praktiziert](https://www.spiegel.de/netzwelt/netzpolitik/nsa-spaehskandal-so-funktionieren-die-abhoeranlagen-in-us-botschaften-a-930392.html)

* **Verteidigung**
  * Gibts nicht. Detektion evtl. via [SnoopSnitch](https://f-droid.org/packages/de.srlabs.snoopsnitch/)

* **Anwendung**
  * Demonstrationen (zB Dresden)
  * Individual-Observationen

---

### Angriffe auf GSM - IMSI Catcher
.center[![IMSI Catcher](img/mobilfunk/imsi-catcher.jpeg)]

---
### UMTS / LTE
* Updates vom 2G Mobilfunk-Protokolls zur **schnelleren Datenübertragung**

  * **GSM** (2G): 40-50 kBit/s max

  * **UMTS** (3G): 200 kBit/s min

  * **LTE** (4G): 20+ MBit/s

???

* Bessere **Transportverschlüsselung**, die schwieriger für Zivilpersonen zu brechen ist

* Daten liegen nach wie vor unverschlüsselt beim Provider (oder dem IMSI Catcher)

---
### "Always on"
* Von "**Smartphones**" erwarten wir "**smarte**" Features

  * Verschneidung persönlicher Informationene mit präzisen Aufenthaltsinformationen (Restaurant-Empfehlungen, Dating-Apps, Navigation, Öffnungszeiten, ...)

  * Instant Messenging

  * Synchronisierungsdienste (Kalender, Cloud-Content, ...)

* Vorraussetzung dafür ist eine **permanente Internetverbindung**

---
### "Always on"
* **Permanente Konnektivität** bedeutet, dass auch **permanent Verkehrsdaten** bei den Providern anfallen, auf welche Behörden Zugriff haben

* Das umständliche (?) "anpingen" von Individuen etwa durch stille SMS zum erstellen **umfangreicher Bewegungsprofile** entfällt, die Daten sind eventuell bei Bedarf im Rahmen der Vorratsdatenspeicherung beim Provider erhältlich

???

* Interessant: Dennoch steigt der Einsatz stiller SMS!

* Gründe unklar und theoretisch vielfältig

---
### "Always on"
[How your innocent smartphone passes on almost your entire life to the secret service](https://www.bof.nl/2014/07/30/how-your-innocent-smartphone-passes-on-almost-your-entire-life-to-the-secret-service/)

<center><img src="img/mobilfunk/metadata_tracking.gif" width="600"></center>

???

Ergebnis einer Untersuchung der anfallenden Metadaten von Ton Siedsma (2014)

---

### Mobilfunküberwachung
1. Der Wandel hin zur Kultur internet-basierter Services durch Smartphones erhöht das Aufkommen von Metadaten **in hohem Maße**

2. Auf Grundlage von über den Tag hochauflösend verteilten Daten ist eine **präzise Analyse unserer Bewegungen und sozialen Zusammenhänge** möglich

3. Es ist davon auszugehen, dass Behörden alle ihnen zur Verfügung stehenden Möglichkeiten nach **Kräften ausnutzen**, um genau diese Analysen vorzunehmen

4. Wer dem entkommen will brauch Alltagspraxen, um den digitalen Fußabdruck zu verkleinern

#### SPOILER
_Es gibt Möglichkeiten sein ["smartes"] Gerät datensparsam zu konfigurieren und zu nutzen. Andererseits macht das aus einem ehemals "smarten" Phone einen mehr oder weniger dummen Kleinstcomputer_
