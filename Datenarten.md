### Arten von Daten

#### Content
* Inhalte von Schriftverkehr und Telefonaten

* Mediendateien (Texte, Mobimaterial, Ton-/Bild-/Videoaufnahmen)

*** Lösung: Verschlüsselung, aber: ***

#### Metadaten
* Wer kommuniziert wann und wo mit wem?

* Welche Webseiten werden in welcher Reihenfolge abgerufen?

* Wo haltet ihr euch auf?

* Wer hat welche Kontakte gespeichert?

* ...

---
.center[![Metadata quote](img/threat_modeling/metadata.jpg)]
